const fs = require('fs-extra');
const path = require('path');
const iconv = require('iconv-lite');
const Transform = require('stream').Transform;
const version = parseInt(process.version.split('.')[0].slice(1), 10);

/**
 * createFixer - creates a Fixer
 *
 * @return {object} Fixer instance
 */
function createFixer() {
	const transformChar = buffer => ({
		221: 'İ',
		240: 'ğ',
		254: 'ş',
		253: 'ı',
		222: 'Ş'
	})[buffer.toJSON().data[0]] || iconv.decode(buffer, 'ISO-8859-1');

	const _transform = (chunk, encoding, callback) =>
		callback(null, chunk.toJSON().data.map(charInt =>
			transformChar(version >= 6 ? Buffer.from([charInt]) : new Buffer([charInt]))).join(''));

	const fix = location => ((fr0m, to) =>
		fs.createReadStream(fr0m)
			.pipe(createFixer())
			.pipe(fs.createWriteStream(to))
		)(location, path.join(path.dirname(location), path.basename(location, path.extname(location)) + '-FIXED' + path.extname(location)));

	return Object.assign(new Transform(), {
		_transform,
		fix
	});
}

const fix = exports.fix = locations => (fixer =>
	locations.forEach(location => fixer.fix(location))
)(createFixer());

exports.createFixer = createFixer;
exports.default = {
	fix,
	createFixer
};

if (require.main === module) {
	fix(process.argv.slice(2).filter(z => z));
}
